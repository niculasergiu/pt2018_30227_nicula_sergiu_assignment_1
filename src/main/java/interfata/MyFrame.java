package interfata;

import model.*;
import javax.swing.*;
import java.awt.*;
//formez interfata in constructor care este apelat mai jos in main
public class MyFrame extends JFrame {
    JTextField af = new JTextField("REZULTAT OPERATIE");
    JButton op1 = new JButton("ADU");
    JButton op2 = new JButton("SCA");
    JButton op3 = new JButton("DER");
    JButton op4 = new JButton("INT");                          /// decralarea elementelor de pe interfata
    JButton op5 = new JButton("INM");

    JTextField p1Af = new JTextField("AFISARE POLINOM");
    JTextField p2Af = new JTextField("AFISARE POLINOM");

    JButton ok1 = new JButton("OK");
    JButton ok2 = new JButton("OK");

    JTextField p1C = new JTextField("INTRODUCE POLINOM");
    JTextField p2C = new JTextField("INTRODUCE POLINOM");

    JButton cl = new JButton("RESET");

    Polinom p1I = new Polinom();
    Polinom p2I = new Polinom();                   ////initializarea elementelor de care avem nevoie pentru operatiisi validarii

    Polinom op = new Polinom();

    boolean k1 = true;
    boolean k2 = true;

    public MyFrame(String title){
        setTitle(title);
        setSize(1000,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);

        af.setBounds(100,470,800,50);

        op1.setBounds(200,350,60,60);
        op2.setBounds(340,350,60,60);
        op3.setBounds(480,350,60,60);
        op4.setBounds(620,350,60,60);
        op5.setBounds(750,350,60,60);

        p1Af.setBounds(100,250,350,50);
        p2Af.setBounds(550,250,350,50);             ///pozitia si dimensiunea la butoane

        ok1.setBounds(190,150,150,50);
        cl.setBounds(430,150,150,50);
        ok2.setBounds(680,150,150,50);

        p1C.setBounds(100,50,350,50);
        p2C.setBounds(550,50,350,50);

        panel.add(af);
        panel.add(op1);
        panel.add(op2);
        panel.add(op3);
        panel.add(op4);
        panel.add(op5);

        panel.add(p1Af);
        panel.add(p2Af);    // introducerea lor in panel
        panel.add(cl);

        panel.add(ok1);
        panel.add(ok2);

        panel.add(p1C);
        panel.add(p2C);


        Font f = new Font("", Font.BOLD, 18);
        af.setHorizontalAlignment(JTextField.CENTER);
        af.setFont(f);
        p1Af.setHorizontalAlignment(JTextField.CENTER);
        p1Af.setFont(f);
        p2Af.setHorizontalAlignment(JTextField.CENTER);          //aspectul la text
        p2Af.setFont(f);
        p1C.setHorizontalAlignment(JTextField.CENTER);
        p1C.setFont(f);
        p2C.setHorizontalAlignment(JTextField.CENTER);
        p2C.setFont(f);


        ok1.addActionListener(new ButonP(p1Af,p1C,p1I,k1));
        ok2.addActionListener(new ButonP(p2Af,p2C,p2I,k2));
        cl.addActionListener(new ButonR(p1I,p2I,op,p1Af,p2Af,p1C,p2C,af));
        op1.addActionListener(new ButonOp(1,p1I,p2I,op,af,k1,k2));     ////evenimentele de pe butoane
        op2.addActionListener(new ButonOp(2,p1I,p2I,op,af,k1,k2));
        op3.addActionListener(new ButonOp(3,p1I,p2I,op,af,k1,k2));
        op4.addActionListener(new ButonOp(4,p1I,p2I,op,af,k1,k2));
        op5.addActionListener(new ButonOp(5,p1I,p2I,op,af,k1,k2));

    }
    public static void main(String[] args) {
        new MyFrame ("Calculator Polinoame");
    }
}

