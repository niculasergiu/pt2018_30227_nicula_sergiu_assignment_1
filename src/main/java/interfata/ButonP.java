package interfata;

import model.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ButonP implements ActionListener
{
    public JTextField tf;
    public JTextField tfa;
    public Polinom p;
    public boolean ok;


    public ButonP(JTextField tf, JTextField tfa, Polinom p, boolean ok)
    {
        this.tf=tf;
        this.tfa=tfa;
        this.p=p;
        this.ok=ok;

    }
// validam polinomul introdus
    public void actionPerformed (ActionEvent e){
        boolean ok = true;
        String pol=tfa.getText();
        p.resetPolinom();
        Citire c = new Citire(pol);

        try {
            c.formareMonoane(p);
        } catch (Exception a) {
            tf.setText("Polinom invalid!!");
            p.resetPolinom();
            ok = false;
        }
        if (ok == true) {
            p.coefZero();
            Collections.sort(p.mon);
            if (p.sizePolinom() > 0) {
                tf.setText(p.afisarePol());
            } else {
                tf.setText("0");
            }
        }
    }
}