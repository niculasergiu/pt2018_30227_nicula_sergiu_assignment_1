package interfata;

import model.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ButonOp implements ActionListener
{

    int nrOp;
    public Polinom a;
    public Polinom b;
    public Polinom op;
    public JTextField af;
    public boolean k1;
    public boolean k2;
// fefectuam operatia dorita
    public ButonOp(int nrOp, Polinom a, Polinom b, Polinom op, JTextField af, boolean k1, boolean k2)
    {
        this.nrOp=nrOp;
        this.a=a;
        this.b=b;
        this.op=op;
        this.af=af;
        this.k1=k1;
        this.k2=k2;

    }

    public void actionPerformed (ActionEvent e) {
        op.resetPolinom();
        if (k1 == true && k2 == true) {
            if (nrOp == 1) {
                Operatii ad = new Operatii(op);
                ad.adunare(a, b);
                op.coefZero();
                Collections.sort(op.mon);
                if (op.sizePolinom() > 0) {
                    af.setText(op.afisarePol());
                } else {
                    af.setText("0");
                }
            }
            if (nrOp == 2) {
                Operatii sc = new Operatii(op);
                sc.scadere(a, b);
                op.coefZero();
                Collections.sort(op.mon);
                if (op.sizePolinom() > 0) {
                    af.setText(op.afisarePol());
                } else {
                    af.setText("0");
                }
            }
            if (nrOp == 3) {
                Operatii der = new Operatii(op);
                der.derivare(a);
                op.coefZero();
                Collections.sort(op.mon);
                if (op.sizePolinom() > 0) {
                    af.setText(op.afisarePol());
                } else {
                    af.setText("0");
                }
            }
            if (nrOp == 4) {
                Operatii inte = new Operatii(op);
                inte.integrare(a);
                op.coefZero();
                Collections.sort(op.mon);
                if (op.sizePolinom() > 0) {
                    af.setText(op.afisarePol());
                } else {
                    af.setText("0");
                }
            }
            if (nrOp == 5) {
                Operatii inm = new Operatii(op);
                inm.inmultire(a, b);
                op.coefZero();
                Collections.sort(op.mon);
                if (op.sizePolinom() > 0) {
                    af.setText(op.afisarePol());
                } else {
                    af.setText("0");
                }
            }
        }
    }
}