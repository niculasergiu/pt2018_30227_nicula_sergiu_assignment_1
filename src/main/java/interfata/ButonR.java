package interfata;

import model.*;
import java.awt.event.*;
import javax.swing.*;

public class ButonR implements ActionListener {

    Polinom a;
    Polinom b;
    Polinom op;

    JTextField p1;
    JTextField p2;

    JTextField p11;
    JTextField p22;

    JTextField af;
// revenim la starea initiala
    public ButonR(Polinom a, Polinom b, Polinom op, JTextField p1, JTextField p2, JTextField p11, JTextField p22, JTextField af)
    {
        this.a=a;
        this.b=b;
        this.op=op;
        this.p1=p1;
        this.p2=p2;
        this.p11=p11;
        this.p22=p22;
        this.af=af;

    }

    public void actionPerformed (ActionEvent e){

        p1.setText("AFISARE POLINOM");
        p2.setText("AFISARE POLINOM");
        p11.setText("INTRODUCE POLINOM");
        p22.setText("INTRODUCE POLINOM");
        af.setText("REZULTAT OPERATIE");
        a.resetPolinom();
        b.resetPolinom();
        op.resetPolinom();
    }
}
