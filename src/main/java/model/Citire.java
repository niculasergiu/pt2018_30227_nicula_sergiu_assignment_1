package model;

import java.util.regex.*;

public class Citire {

    public String s;
    public int marime;

    public Citire(String s) {
        this.s = s;
    }
// cu ajutorul Regex impartim polinomul dat in monoame
    public void formareMonoane(Polinom p1) throws Exception {
        String line = s;
        if(line.charAt(0)!='-') {
            line = "+" + line;
        }
        String pattern = "([-+])(\\d+)?(X(\\^(\\d+))?)?";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(line);
        while (m.find()) {
            if (m.group(2) != null || m.group(3) != null) {
                int coef, exp;
                if (m.group(2) == null) {
                    coef = 1;
                } else {
                    coef = Integer.parseInt(m.group(2));
                }
                String semn = m.group(1);
                if (semn == null) {
                    semn = "+";
                }
                if (semn.equals("-")) {
                    coef = -coef;
                }
                if (m.group(4) == null) {
                    exp = 1;
                    if (m.group(3) == null) {
                        exp = 0;
                    }
                } else {
                    exp = Integer.parseInt(m.group(5));
                }
                Monom monom = new Monom(coef, exp);
                p1.upPolinom(monom, 1);
                marime = marime + m.group(0).length();
            }
        }
        if (marime != line.length()) {
            throw new Exception();
        }
    }
}


