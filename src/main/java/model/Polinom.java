package model;

import java.util.*;


public class Polinom {
    public List<Monom> mon;//construim Polinomul fiind o lista de monoame


    public Polinom() {
        mon = new ArrayList<Monom>();
    }

//metoda prin care trasformam polinomul:
    // daca o este egal cu 1 la exp egali se aduna coeficientul, daca nu se scad coeficienti la ecponent egal
    //daca exponentul nu este egal se creaza obiect nou ( o diferit de 1 se schimba semnul la coeficientul din monom)
    public void upPolinom(Monom monN, int op) {
        int i=0;
     for (Monom a : mon) {
            if (monN.exp == a.exp) {
                if (op ==1) {
                    a.coef = a.coef + monN.coef;
                    i++;
                } else {
                    a.coef = a.coef - monN.coef;
                    i++;

                }
            }
        }
        if(op==1) {
         if(i==0){
            mon.add(monN);
         }
        }
        else
        {
            if(i==0){
            monN.coef=-monN.coef;
            mon.add(monN);
            }
        }
    }
// se foloseste pentru a afisa pe interfata polinomul
    public String afisarePol() {
        boolean ok=true;
        String s="";
        for (Monom a : mon) {
           s=s+a.toString(ok);
            ok=false;
        }
        return s;
    }
    //returneaza polinonum
    public List<Monom> getListaMonom() {
        return this.mon;
    }
    //reseteaza polinomul
    public void resetPolinom(){
        Polinom reset = new Polinom();
        for (Monom a : mon) {
            reset.upPolinom(a,1);
        }

        mon.removeAll(reset.getListaMonom());

    }
    //elimina monoamele cu coeficient 0
    public void coefZero(){
        Polinom zero = new Polinom();
        for (Monom a : mon) {
            if(a.coef==0) {
                zero.upPolinom(a, 1);
            }
        }

        mon.removeAll(zero.getListaMonom());

    }
    // returneaza dimensiunea polinomului
    public int sizePolinom(){
        return mon.size();
    }
}

