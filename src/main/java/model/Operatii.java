package model;

public class Operatii {

    public Polinom p3;

    public Operatii(Polinom p3) {
        this.p3 = p3;

    }
//se adauga pe rand cei 2 polinomi in alt polinom care rezulta suma celor 2
    public void adunare(Polinom p1, Polinom p2) {
        for (Monom a : p1.getListaMonom()){
            Monom m = new Monom(a.coef,a.exp);
            p3.upPolinom(m,1);
        }
        for (Monom a : p2.getListaMonom()){
            Monom m = new Monom(a.coef,a.exp);
            p3.upPolinom(m,1);
        }
    }
//se adauga primul si a2lea are op setat pe 1, adica la exponenti egali va face scadere intre coeficienti si la monom nou ii va schimba semnul la coeficient
    public void scadere(Polinom p1, Polinom p2) {
        for (Monom a : p1.getListaMonom()){
            Monom m = new Monom(a.coef,a.exp);
            p3.upPolinom(m,1);
        }
        for (Monom a : p2.getListaMonom()){
            Monom m = new Monom(a.coef,a.exp);
            p3.upPolinom(m,2);
        }
    }
// se face derivare pe primul polinom
    public void derivare(Polinom p1) {
        for (Monom a : p1.getListaMonom()) {
            if (a.exp == 0){}
            else {
                Monom m = new Monom((a.coef * a.exp), (a.exp - 1));
                p3.upPolinom(m, 1);
            }
        }
    }
    // se face integrarea pe primul polinom
        public void integrare(Polinom p1){
            for (Monom a : p1.getListaMonom()) {
                float x = (a.coef / (a.exp + 1));
                Monom m = new Monom(x,(a.exp + 1));
                p3.upPolinom(m, 1);
            }
    }
    //se inmulteste coeficientele si aduna exponentul din monom rand pe rand din cei 2 polinomi
    public void inmultire(Polinom p1, Polinom p2) {
        for (Monom a : p1.getListaMonom())
        {
            for (Monom b : p2.getListaMonom()){
                Monom m = new Monom((a.coef * b.coef),(a.exp + b.exp));
                p3.upPolinom(m,1);
        }

        }
    }
}
