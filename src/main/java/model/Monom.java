package model;

import java.text.DecimalFormat;
//clasa fundamentalÄƒ a proiectului ce defineÈ™te structura de monom.
public class Monom implements Comparable {

    float coef;
    public int exp;
    private static DecimalFormat formatCoef = new DecimalFormat("#.##");

    public Monom(float coef, int exp){
        this.coef=coef;
        this.exp=exp;

    }
    //afisare monoame negative
    public String nrNegative()
    {
        if (coef == -1 && exp == 1) {
            return "-X";
        }
        if (coef == -1 && exp > 1) {
            return "-X^" + exp;
        }
        if (coef <= 1 && exp == 0) {
            return formatCoef.format(coef);
        }
        if (coef <= 1 && exp == 1) {
            return formatCoef.format(coef) + "X";
        }
        return formatCoef.format(coef) + "X^" + exp;

    }
    //metoda princilata de afisare
    public String toString(boolean ok){
        if(coef>0) {
            if(coef==1 && exp ==1)
                if(ok==false){
                return "+X";
            }
            else
            {
                return"X";
            }
            if(coef==1 && exp >1)
                if(ok==false) {
                    return "+X^" + exp;
                }
                else
                {
                    return "X^" +exp;
                }

            if(coef>=1 && exp == 0)
                if(ok==false) {
                    return "+" + formatCoef.format(coef);
                }
            else
                {
                    return formatCoef.format(coef);
                }
            if(coef>=1 && exp == 1)
                if(ok==false) {
                    return "+" + formatCoef.format(coef) + "X";
                }
                else
                {
                    return  formatCoef.format(coef) + "X";
                }
                if(ok==false) {
                    return "+" + formatCoef.format(coef) + "X^" + exp;
                }
                else
                {
                    return formatCoef.format(coef) + "X^" + exp;
                }
        }
        else {
            return nrNegative();

        }
    }
// ordoneaza monoamele descrescator dupa expoment
    public int compareTo (Object object)
    {
        Monom e = (Monom) object;
        return -(exp-e.exp);
    }
}
