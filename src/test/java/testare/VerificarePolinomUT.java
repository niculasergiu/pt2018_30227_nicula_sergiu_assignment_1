package testare;

import static org.junit.Assert.*;
import org.junit.Test;


public class VerificarePolinomUT
{
    @Test
    public void VerificP()
    {
        Testare testare = new Testare();

        assertTrue(testare.esteValid("X","X"));
        assertTrue(testare.esteValidOp("X","X","2X",1));
        assertTrue(testare.esteValidOp("X","X","0",2));
        assertTrue(testare.esteValidOp("X","X","1",3));
        assertTrue(testare.esteValidOp("X","X","0.5X^2",4));
        assertTrue(testare.esteValidOp("X","X","X^2",5));

        assertTrue(testare.esteValid("-X","-X"));
        assertTrue(testare.esteValidOp("-X","-X","-2X",1));
        assertTrue(testare.esteValidOp("-X","-X","0",2));
        assertTrue(testare.esteValidOp("-X","-X","-1",3));
        assertTrue(testare.esteValidOp("-X","-X","-0.5X^2",4));
        assertTrue(testare.esteValidOp("-X","-X","X^2",5));

        assertFalse(testare.esteValidOp("-X","-X ","X^2",5));

        assertTrue(testare.esteValid("5","5"));
        assertTrue(testare.esteValid("-5","-5"));

        assertTrue(testare.esteValid("5X","5X"));
        assertTrue(testare.esteValid("-5X","-5X"));

        assertTrue(testare.esteValid("5X^2","5X^2"));
        assertTrue(testare.esteValid("-5X^2","-5X^2"));

        assertTrue(testare.esteValid("5X^2-5X^2+X","5X^2-5X^2+X"));
        assertTrue(testare.esteValid("-5X^2-5X^2+X","-5X^2-5X^2+X"));

        assertFalse(testare.esteValidOp("-X","-X ","X^2",5));
        assertFalse(testare.esteValidOp("X","X ","0.5X^2",4));

        assertFalse(testare.esteValid("",""));
        assertFalse(testare.esteValid("5X^2- 5X^2+X","5X^2- 5X^2+X"));
        assertFalse(testare.esteValid("5X^2-5aX^2+X","5X^2-5aX^2+X"));
        assertFalse(testare.esteValid("5X^2-5X^2+X.","5X^2-5X^2+X."));
        assertFalse(testare.esteValid("5X^2-5X^+X","5X^2-5X^+X"));
        assertFalse(testare.esteValid("orice inafar de polinom corect","orice inafar de polinom corect"));

    }

}
