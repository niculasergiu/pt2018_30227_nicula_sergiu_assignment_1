package testare;


import java.util.*;
import model.*;

public class Testare {
    // se initializeaza polinomii care avem nevoie pentru verificare si validarile
    boolean k1 = true;
    boolean k2 = true;

    boolean ok=false;

    Polinom p1I = new Polinom();
    Polinom p2I = new Polinom();

    Polinom op = new Polinom();

    public boolean esteValid(String a,String b){

        Citire c1 = new Citire(a);
        Citire c2 = new Citire(b);

        try {
            k1=true;
            c1.formareMonoane(p1I);
        } catch (Exception e) {
            p1I.resetPolinom();
            k1 = false;
        }

        try {
            k2=true;
            c2.formareMonoane(p2I);
        } catch (Exception e) {
            p2I.resetPolinom();
            k2 = false;
        }

        if( k1==true && k2==true)
            return true;
        else
            return false;
    }

    public boolean esteValidOp(String a,String b,String c,int nrOp){
        ok=false;
        p1I.resetPolinom();
        p2I.resetPolinom();
        op.resetPolinom();
        Citire c1 = new Citire(a);
        Citire c2 = new Citire(b);

        try {
            k1=true;
            c1.formareMonoane(p1I);
        } catch (Exception e) {
            p2I.resetPolinom();
            k1 = false;
        }

        try {
            k2=true;
            c2.formareMonoane(p2I);
        } catch (Exception e) {
            p2I.resetPolinom();
            k2 = false;
        }

        if(k1==true && k2==true){
            if (nrOp == 1) {
                Operatii ad = new Operatii(op);
                ad.adunare(p1I,p2I);
                op.coefZero();
                Collections.sort(op.mon);
                String d;
                if (op.sizePolinom() > 0)
                    d=op.afisarePol();
                else
                    d="0";
                if(c.equals(d))
                    ok=true;
                else
                    ok=false;
            }
            if (nrOp == 2) {
                Operatii sc = new Operatii(op);
                sc.scadere(p1I,p2I);
                op.coefZero();
                Collections.sort(op.mon);
                String d;
                if (op.sizePolinom() > 0)
                    d=op.afisarePol();
                else
                    d="0";
                if(c.equals(d))
                    ok=true;
                else
                    ok=false;
            }
            if (nrOp == 3) {
                Operatii der = new Operatii(op);
                der.derivare(p1I);
                op.coefZero();
                Collections.sort(op.mon);
                String d;
                if (op.sizePolinom() > 0)
                    d=op.afisarePol();
                else
                    d="0";
                if(c.equals(d))
                    ok=true;
                else
                    ok=false;
            }
            if (nrOp == 4) {
                Operatii inte = new Operatii(op);
                inte.integrare(p1I);
                op.coefZero();
                Collections.sort(op.mon);
                String d;
                if (op.sizePolinom() > 0)
                    d=op.afisarePol();
                else
                    d="0";
                if(c.equals(d))
                    ok=true;
                else
                    ok=false;
            }
            if (nrOp == 5) {
                Operatii inm = new Operatii(op);
                inm.inmultire(p1I,p2I);
                op.coefZero();
                Collections.sort(op.mon);
                String d;
                if (op.sizePolinom() > 0)
                    d=op.afisarePol();
                else
                    d="0";
                if(c.equals(d))
                    ok=true;
                else
                    ok=false;
            }
        }
        return ok;
    }
}

